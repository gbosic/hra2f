#!/bin/bash
#converts text to PostScript or Portable Document Format
# example usage
# ./txtToPsPdf.sh input.txt "Title Name"
# output is a .ps and a .pdf file

input="$1" # input file
title="$2" # title
awk '/./' "$input" > "$input.tmp"
# replace unprintable characters
sed -i -e "s/…/.../g;
           s/—/--/g;
           s/–/--/g;
#           s/\//|/g;
           s/\\\\/|/g;
           s/•/*/g;
           s/”/\"/g;
           s/\f//g;
           s/“/\"/g;
           s/‘/\'/g;
           s/’/\'/g;" "$input.tmp"
# remove non ASCII characters
cat "$input.tmp" | tr -cd '\000-\177' > "$input.tmp2"
mv "$input.tmp2" "$input.tmp"
cat "$input.tmp" | tr -d '\200-\377' > "$input.tmp2"
mv "$input.tmp2" "$input.tmp"
cat "$input.tmp" | ./unwrap.js > "$input.tmp2"
mv "$input.tmp2" "$input.tmp"
a2ps --prologue=bold -B -R --print-anyway 1 --rows 4 --columns 6 \
  --chars-per-line 60 \
   --center-title="%p./%p#" --header="$title. %s./%s#" \
   "$input.tmp" -o "$input.ps"

# make pdf
ps2pdf "$input.ps"

# fixes for page 311
#cat "$input.ps" | sed -e '/^(was in paradise that day.)/d' > "$input.adj.ps"
