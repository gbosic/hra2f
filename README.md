# hra2f - Archival Scripts

## Introudction
This folder has some scripts for archiving text and pdf documents

These are all scripts that can run in any [https://en.wikipedia.org/wiki/POSIX#POSIX-certified](POSIX) environment, such as Linux, Android via Termux or Cygwin on Microsoft, assuming the dependenceies are installed. 

Do not print on cheap film, since it degrades quickly. Use acid free paper, or even waterproof or water-resistant paper, or polyester.
Do not use inkjet printers, the dyes they use will quickly fade, instead use laser printers with carbon black toner. 

It is best to print single sided if possible, because over time the letters can smear together if they are facing each other. 

I know the font size is pretty small, and for some older people it may not be legible without magnification.
You can change these scripts so there are fewer pages per sheet, though that will increase the total number of pages printed. 

## txtToPsPdf.sh
This  file converts .txt files into condensed sheets. Similar in size to microfiche but generally for printing on paper rather than polyester. Though you could print them on polyester if you want. This is great for archiving text, however it will not archive images, for that look at pdfcompression.sh
This is for ideally for printing single sided archival.  

The dependencies are a2ps, nodejs and (optionally) ps2pdf 

On Ubuntu:
```
apt install a2ps ghostscript nodejs
```
The first argument is the text file, and the second argument quoted is the name, can also put in the publication date and topic.  For example:

Example usage:
```
./txtToPsPdf.sh "abook.txt" "A very important book by author 2022CE about archival"
```

Output will be abook.ps and abook.pdf

## pdfcompression.sh
This file converts .pdf files so can fit many pages on a single sheet. This is best for books that have many pictures. 
Downside is that it doesn't have a title per sheet or sheet number, so make sure that at least the pdf pages have numbers. 
This is also ideal for single sided archival.

The dependenies are gs (ghostscript) and pdfjam.

On Ubuntu:
```
apt install ghostscript texlive-extra-utils
```

Example usage:
```
./pdfcompression.sh book.pdf
```

output file will be book.pdf.pdf

## pdfToBook.sh
This will produce a printable book, that you can carry around with you. This is ideal for making usable/readable reference books for regular usage. Wheras the other files are for making information dense archives that can be deciphered by future generations. For example can use this script to print a bindable bible. 

Dependencies are pdfbook2 and nodejs

On Ubuntu:
```
apt install texlive-extra-utils nodejs
```

There are several steps to this process. First you will need a pdf file. And you need to know the number of pages that pdf file has. 

then  assuming you have 350 pages
```
./findLeaflet.js 
./findLeaflet.js 350
can use leaflet size of 4, there will be  87 leaflets, and 2 vacant pages at the termination
can use leaflet size of 8, there will be  43 leaflets, and 2 vacant pages at the termination
can use leaflet size of 16, there will be  21 leaflets, and 2 vacant pages at the termination
can use leaflet size of 32, there will be  10 leaflets, and 2 vacant pages at the termination
can use leaflet size of 44, there will be  7 leaflets, and 2 vacant pages at the termination
can use leaflet size of 88, there will be  3 leaflets, and 2 vacant pages at the termination
can use leaflet size of 176, there will be  1 leaflets, and 2 vacant pages at the termination
can use leaflet size of 352, there will be  0 leaflets, and 2 vacant pages at the termination
```

Generally for expediency of binding while still taking the shape of a book, I like to have around 5 leaflets (booklets/signatures). 

so lets say I want to end up with 7 leaflets, then I will use leaflet size of 44

then
```
./pdfToBook.sh book.pdf 44
```

Then the result will be book-book.pdf
