#!/usr/bin/env node
function isUpperCase (input) {  
  return input === String(input).toUpperCase()
}
process.stdin.on('readable', () => {
	let chunk;
	let paragraph = new String();
	while ((chunk = process.stdin.read()) !== null) {
		chunk = chunk.toString().replace(/\f/g,'');
		let lines = chunk.split('\n');
		paragraph = new String();
		lines.map((line) => {
			line = line.replace(/\s\s+/g, ' ');
//			console.log(`line[-1] == "." ${line[-1] == "."} isUpperCase(line[0]) ${isUpperCase(line[0]) && /[0-9]/.test(line[0]) == false} line ${JSON.stringify(line)}`);
			if (line[line.length-1] == ".") {
				paragraph += line;
				process.stdout.write(`${paragraph}\n`);
				paragraph = new String();
			}
			else if (isUpperCase(line[0])  && /[0-9]/.test(line[0]) == false  && line.length > 5) {
				if (paragraph.length > 0) {
				process.stdout.write(`${paragraph}\n`);
				}
				paragraph = line;
			}
			else {
				paragraph += line.replace(/[\r\n]/,'') + " ";
			//	console.log(JSON.stringify(paragraph))
			}
		});
		process.stdout.write(`${paragraph}\n`);
	}
});
