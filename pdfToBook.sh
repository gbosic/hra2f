#!/bin/bash
# arguments $1 pdf, $2 signatures
book=$1
signatures=$2
pdfbook2 "$book" -s --paper=letterpaper  --top-margin=10 --bottom-margin=5 --outer-margin=20 --signature=$signatures --inner-margin=80
