#!/bin/bash
# usage give pdf file as second argument
file=$@
pdfjam --nup 6x4 "$file" -o "$file.tmp.pdf"
pdfjam --fitpaper true --trim "0cm -0.8cm -0cm -0.8cm" "$file.tmp.pdf" -o "$file.tmp2.pdf"
gs \
 -sOutputFile="$file.pdf" \
 -sDEVICE=pdfwrite \
 -sColorConversionStrategy=Gray \
 -dProcessColorModel=/DeviceGray \
 -dCompatibilityLevel=1.4 \
 -dNOPAUSE \
 -dBATCH \
 "$file.tmp2.pdf"
#rm $1.tmp.pdf
