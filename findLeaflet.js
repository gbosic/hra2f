#!/usr/bin/env node
// first argument is number of pages
const pages = process.argv[2];
const elite = {signature: 0, quantity: 0, remains: 0};
for (let signature = 4; signature <= pages+4; signature += 4) {
	const quantity = pages/signature + 1;
	const remains = quantity - parseInt(quantity.toString());
	//console.log(`${pages}/${signature} = ${quantity}`);
	if (elite.remains < remains || remains == 0) {
		elite.signature = signature; elite.remains = remains, elite.quantity = quantity;
		//		console.log(JSON.stringify(elite));
		const vacant_pages = elite.remains == 0? 0: Math.round((1-elite.remains)*elite.signature);
		console.log(`can use leaflet size of ${elite.signature}, there will be ` +
			` ${parseInt(elite.quantity.toString())} leaflets, and `+
		`${vacant_pages} vacant pages at the termination`);
	}
}
